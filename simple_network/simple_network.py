import h5py
import numpy as np

import spektral as spk
from spektral.layers import GraphConv, GraphAttention
from tensorflow.keras import activations, initializers, regularizers, constraints
from tensorflow.keras.layers import Dense, Input, LSTM, concatenate, Layer, LSTMCell, \
    RNN, Flatten, Conv2D, GRU, Conv3D, MaxPool3D, MaxPooling2D, UpSampling2D, Cropping2D, \
    ConvLSTM2D, SpatialDropout2D, BatchNormalization, Reshape
from tensorflow.keras import Model
import tensorflow as tf
import os

from utils import load_data, load_testing_data, plot_history
TAMANHO = 10

class SimpleNetwork:

    def build(self):

        images = Input(shape=(12, TAMANHO, TAMANHO, 9))  # Variable-length sequence of 40x40x1 frames
        print("entrada: ", images)
        x = ConvLSTM2D(filters=40, kernel_size=(3, 3), padding="same", return_sequences=True, data_format="channels_last")(images)
        print("primeiro: ", x.shape)
        x = BatchNormalization()(x)
        x = ConvLSTM2D(filters=40, kernel_size=(3, 3), padding="same", return_sequences=True)(x)
        x = BatchNormalization()(x)
        x = ConvLSTM2D(filters=40, kernel_size=(3, 3), padding="same", return_sequences=True)(x)
        x = BatchNormalization()(x)
        x = ConvLSTM2D(filters=40, kernel_size=(3, 3), padding="same", return_sequences=True)(x)
        x = BatchNormalization()(x)
        print("ultimo batch: ", x.shape)
        x = MaxPool3D((12, 1, 1))(x)
        print("shape maxpool: ", x.shape)
        # memoria excede
        # x = x[:, 0, :, :, :]
        x = Reshape((TAMANHO, TAMANHO, 40))(x)
        print("shape final: ", x.shape)
        x = Conv2D(filters=9, kernel_size=(3, 3), activation="sigmoid", padding="same")(x)
        print("shape conv: ", x.shape)

        model = Model(inputs=[images], outputs=[x])

        return model

if __name__ == "__main__":

    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

    if tf.test.gpu_device_name():
        print('Default GPU Device:{}'.format(tf.test.gpu_device_name()))

    else:

        print("Please install GPU version of TF")

    # gpus = tf.config.experimental.list_physical_devices('GPU')
    # if gpus:
    #     try:
    #         tf.config.experimental.set_virtual_device_configuration(gpus[0], [
    #             tf.config.experimental.VirtualDeviceConfiguration(memory_limit=1024)])
    #     except RuntimeError as e:
    #         print(e)
    # tf.config.threading.set_intra_op_parallelism_threads(2)
    # tf.config.threading.set_inter_op_parallelism_threads(2)

    training_file_dir = "/media/claudio/Acer/Users/claud/Downloads/doutorado/iarai/BERLIN/training/"
    validation_file_dir = "/media/claudio/Acer/Users/claud/Downloads/doutorado/iarai/BERLIN/validation"
    testing_file_dir = "/media/claudio/Acer/Users/claud/Downloads/doutorado/iarai/BERLIN/testing/"
    static_filename = "/media/claudio/Acer/Users/claud/Downloads/doutorado/iarai/BERLIN/BERLIN_static_2019.h5"

    # ler os dados do diretorio de treinamento (um arquivo só por enquanto)

    timestep = 12
    print("Reading training data")
    #x_train, y_train = load_data(training_file_dir, timestep)
    """usar as matrizes abaixo apenas para ver se a rede neural esta compilando"""
    x_train = np.random.rand(24, 12, TAMANHO, TAMANHO, 9)
    y_train = np.random.rand(24, TAMANHO, TAMANHO, 9)
    print("Reading testing data")
    #validation_data = load_data(validation_file_dir, timestep)
    # ler os dados do diretorio de teste (um arquivo só por enquanto)
    #testing_data = load_testing_data(testing_file_dir)


    model = SimpleNetwork().build()
    #print(model.summary())
    model.compile(metrics=['accuracy'], optimizer='adadelta', loss="mse")



    hi = model.fit(x=x_train, y=y_train, epochs=4, batch_size=2)


    plot_history(hi)
    #acc = model.evalua5te(x=x_test, y=y_test,batch_size=2, use_multiprocessing=True)
    #print("Acc: ", acc)


    # with h5py.File(static_filename, "r") as f:
    #     # List all groups
    #     print("Keys: %s" % f.keys())
    #     keys = list(f.keys())
    #     for key in keys:
    #
    #         # Get the data
    #         group = f[key]
    #         print("tipo inicial: ", type(group))
    #         print("dados: ", group.name)
    #         valor = group[()]
    #         print("tamanho: ", valor.shape)
    #         print("tipo: ", type(valor))