import os
import h5py
import numpy as np
import matplotlib.pyplot as plt

def split_x_y(data, timestep):
    x = []
    y = []
    # x_train = data[:data.shape[0] - 1, :, :, :]
    # y_train = data[1:data.shape[0], :, :, :]
    # x_train = np.asarray([x_train[i:(i + 2), :, :, :] for i in range(12)])
    # y_train = np.asarray([y_train[i:(i + 2), :, :, :] for i in range(12)])
    # loop 12 by 12
    for i in range(int(288/timestep)):
        x.append(data[i:i+timestep, :, :, :])
        y.append(data[i+timestep+1, :, :, :])

    x = np.array(x)
    y = np.array(y)
    print("x and y shape: ", x.shape, y.shape)
    return x, y

def load_data(dir, timestep) -> (np.ndarray, np.ndarray):
    files = [os.listdir(dir)[0]]


    for file in files:
        with h5py.File(dir+file, "r") as f:
            # List all groups
            keys = list(f.keys())
            for key in keys:

                # Get the data
                group = f[key]
                data = group[()]
                data = np.stack(data, axis=0)

    print("original size: ", data.shape)
    data = data.astype(np.float32)
    data = np.apply_along_axis(lambda e: e / 255, -1, data)
    x, y = split_x_y(data, timestep)
    return x, y

def load_testing_data(dir):
    files = [os.listdir(dir)[0]]

    print("Reading testing data")
    for file in files:
        with h5py.File(dir+file, "r") as f:
            # List all groups
            keys = list(f.keys())
            for key in keys:

                # Get the data
                group = f[key]
                data = group[()]
                data = np.stack(data, axis=0)

    print("original size: ", data.shape)
    data = data[0]
    data = data.astype(np.float32)
    data = np.apply_along_axis(lambda e: e / 255, -1, data)

    return data

def plot_history(history):
    print(history.history.keys())
    # summarize history for accuracy
    plt.plot(history.history['accuracy'])
    #plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()
    # summarize history for loss
    plt.plot(history.history['loss'])
    #plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()
