import pandas as pd
import h5py
import statistics as st
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns

def boxplot(df, name, title):
    plt.figure(figsize=(14, 12))
    # print("final: ", df['value'].describe())
    figure = sns.boxplot(x="channel", y="value", data=df)
    figure.set_title(title)
    figure = figure.get_figure()
    plt.subplots_adjust(bottom=0.32, left=0.28)
    figure.savefig(name + "_boxplot.png", dpi=400, bbox_inches='tight', figsize=(110, 110))
    plt.show()

def heatmap(df, name, title):
    # Using Pearson Correlation
    plt.figure(figsize=(14, 12))
    cor = df.corr()
    figure = sns.heatmap(cor, annot=True, cmap=plt.cm.Reds)
    # print("final: ", df['value'].describe())
    # figure = sns.boxplot(x="dimension", y="value", data=df)
    figure = figure.get_figure()
    plt.subplots_adjust(bottom=0.32, left=0.28)
    # figure.savefig("static_boxplot.png", dpi=400, bbox_inches='tight', figsize=(110, 110))
    figure.savefig(name + "_heatmap.png", dpi=400, bbox_inches='tight', figsize=(110, 110))
    plt.show()

def dynamic_channels_plot(data):
    print("Dynamic channels plot")

    flatten_channels_dict = {i: [] for i in range(9)}
    flatten_channels = np.array([])
    channel_id = np.array([])
    for channel in range(data.shape[3]): # 9
        channel_data = data[:, :, :, channel]
        # channel_id.append(channel)
        # flatten_channels.append(st.mean(channel_data.flatten().tolist()))
        channel_data = channel_data.flatten()
        channel_data = channel_data[:int(channel_data.shape[0]/10)]
        
        channel_id = np.concatenate((channel_id, np.array([channel]*channel_data.shape[0])))
        flatten_channels_dict[channel] = channel_data
        flatten_channels = np.concatenate((flatten_channels, channel_data))
        print(channel_data.shape)

    # boxplot_df = pd.DataFrame({"channel": channel_id, "value": flatten_channels})
    # boxplot_df['channel'] = boxplot_df['channel'].astype('int32')
    # boxplot(boxplot_df, "dynamic", "Boxplot Training (10% random sample)")

    heatmap_df = pd.DataFrame(flatten_channels_dict)
    heatmap(heatmap_df, "Channels' correlation (training dataset)", "")


def static_to_df(data):
    channels = {0: "junção media de estradas", 1: "soma da junção de estradas" , 2: "eat, drink and entertainment", 3: "hospital", 4: "parking",
                5: "shopping", 6: "transport"}
    flatten_data_channels = {"junção media de estradas": [], "soma da junção de estradas": [] ,
                             "eat, drink and entertainment": [], "hospital": [],
                             "parking": [], "shopping": [], "transport": []}
    for channel in range(data.shape[2]):
        print("Channel: ", channel)
        name = channels[channel]
        print("Nome: ", name)
        data_channel = data[:,:,channel]
        #print(data_channel)
        flatten = []
        for v in data_channel:
            #print(v)
            v = list(v)
            v = [e/255 for e in v]
            flatten = flatten + v
        flatten_data_channels[name] = flatten
        #print("Describe: ", pd.Series(flatten).describe())

    df = pd.DataFrame(flatten_data_channels)

    return df



if __name__ == "__main__":

    dynamic_file_file_name = "/media/claudio/Acer/Users/claud/Downloads/doutorado/iarai/BERLIN/training/2019-01-01_berlin_9ch.h5"
    # df = pd.read_hdf("/media/claudio/Acer/Users/claud/Downloads/doutorado/iarai/BERLIN/training/2019-01-01_berlin_9ch.h5")
    # print(df)
    static_filename = "/media/claudio/Acer/Users/claud/Downloads/doutorado/iarai/BERLIN/BERLIN_static_2019.h5"

    with h5py.File(dynamic_file_file_name, "r") as f:
        # List all groups
        print("Keys: %s" % f.keys())
        key = list(f.keys())[0]

        # Get the data
        group = f[key]
        dinamico = group[()]
        dinamico = dinamico[0:]
        dinamico = np.strack(dinamico, axis=0)
        print("tamanho: ", dinamico.shape)
        print("tipo: ", type(dinamico))
        print("indexando0: ", dinamico[0, :, :, 0].shape)
        print("indexando1: ", dinamico[0, :, :, 8].shape)
        dynamic_channels_plot(dinamico)

    with h5py.File(static_filename, "r") as f:
        # List all groups
        print("Keys: %s" % f.keys())
        key = list(f.keys())[0]

        # Get the data
        group = f[key]
        estatico = group[()]
        estatico = estatico[0:]
        estatico = np.strack(estatico, axis=0)
        print("tamanho: ", estatico.shape)
        # (495, 436, 7)
        print("tipo: ", type(estatico))
        #channels_plots(valor)
