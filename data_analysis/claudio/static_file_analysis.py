import pandas as pd
import h5py

import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns


def channels_plots(data):
    channels = {0: "junção media de estradas", 1: "soma da junção de estradas" , 2: "eat, drink and entertainment", 3: "hospital", 4: "parking",
                5: "shopping", 6: "transport"}
    flatten_data_channels = {"junção media de estradas": [], "soma da junção de estradas": [] ,
                             "eat, drink and entertainment": [], "hospital": [],
                             "parking": [], "shopping": [], "transport": []}
    for channel in range(data.shape[2]):
        print("Channel: ", channel)
        name = channels[channel]
        print("Nome: ", name)
        data_channel = data[:,:,channel]
        #print(data_channel)
        flatten = []
        for v in data_channel:
            #print(v)
            v = list(v)
            v = [e/255 for e in v]
            flatten = flatten + v
        flatten_data_channels[name] = flatten
        #print("Describe: ", pd.Series(flatten).describe())

    df = pd.DataFrame(flatten_data_channels)

    # colunas = df.columns[2:]
    # #colunas = [colunas[1]]
    # new_data = []
    # new_colunas = []
    # for c in colunas:
    #     d = df[c].tolist()
    #     coluna = [c]*df.shape[0]
    #     new_data = new_data + d
    #     new_colunas = new_colunas + coluna
    #
    # df = pd.DataFrame({"dimension": new_colunas, "value": new_data})

    # Using Pearson Correlation
    plt.figure(figsize=(14, 12))
    cor = df.corr()
    figure = sns.heatmap(cor, annot=True, cmap=plt.cm.Reds)
    #print("final: ", df['value'].describe())
    #figure = sns.boxplot(x="dimension", y="value", data=df)
    figure = figure.get_figure()
    plt.subplots_adjust(bottom=0.32,left=0.28)
    #figure.savefig("static_boxplot.png", dpi=400, bbox_inches='tight', figsize=(110, 110))
    figure.savefig("static_heatmap2.png", dpi=400, bbox_inches='tight', figsize=(110, 110))
    plt.show()



if __name__ == "__main__":

    dynamic_file_file_name = "/media/claudio/Acer/Users/claud/Downloads/doutorado/iarai/BERLIN/training/2019-01-01_berlin_9ch.h5"
    # df = pd.read_hdf("/media/claudio/Acer/Users/claud/Downloads/doutorado/iarai/BERLIN/training/2019-01-01_berlin_9ch.h5")
    # print(df)
    static_filename = "/media/claudio/Acer/Users/claud/Downloads/doutorado/iarai/BERLIN/BERLIN_static_2019.h5"

    with h5py.File(static_filename, "r") as f:
        # List all groups
        print("Keys: %s" % f.keys())
        keys = list(f.keys())
        for key in keys:

            # Get the data
            group = f[key]
            print("tipo inicial: ", type(group))
            print("dados: ", group.name)
            valor = group[()]
            print("tamanho: ", valor.shape)
            # (495, 436, 7)
            print("tipo: ", type(valor))
            channels_plots(valor)
