import pandas as pd
import h5py

if __name__ == "__main__":

    dynamic_file_file_name = "/media/claudio/Acer/Users/claud/Downloads/doutorado/iarai/BERLIN/training/2019-01-01_berlin_9ch.h5"
    # df = pd.read_hdf("/media/claudio/Acer/Users/claud/Downloads/doutorado/iarai/BERLIN/training/2019-01-01_berlin_9ch.h5")
    # print(df)
    static_filename = "/media/claudio/Acer/Users/claud/Downloads/doutorado/iarai/BERLIN/BERLIN_static_2019.h5"

    with h5py.File(dynamic_file_file_name, "r") as f:
        # List all groups
        print("Keys: %s" % f.keys())
        keys = list(f.keys())
        for key in keys:

            # Get the data
            group = f[key]
            print("tipo inicial: ", type(group))
            print("dados: ", group.name)
            valor = group[()]
            print("tamanho: ", valor.shape)
            print("tipo: ", type(valor))
            print("indexando0: ", valor[0, :,:,0].shape)
            print("indexando1: ", valor[0, :, :, 8].shape)

    with h5py.File(static_filename, "r") as f:
        # List all groups
        print("Keys: %s" % f.keys())
        keys = list(f.keys())
        for key in keys:

            # Get the data
            group = f[key]
            print("tipo inicial: ", type(group))
            print("dados: ", group.name)
            valor = group[()]
            print("tamanho: ", valor.shape)
            print("tipo: ", type(valor))